package com.example.petsadoption.model

data class Cat (
    var name:String="",
    var ras:String="",
    var gender:String="",
    var lokasi:String="",
    var photo:Int=0,
    var detail:String="",
    var age:String=""
)