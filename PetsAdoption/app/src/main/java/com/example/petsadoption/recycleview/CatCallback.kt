package com.example.petsadoption.recycleview

import android.graphics.drawable.DrawableContainer
import android.widget.ImageView
import android.widget.TextView

interface CatCallback {
    fun onCatItemClick(pos:Int, imgContainer: ImageView, imgCat:ImageView, catname:TextView,
                       catras: TextView, catgender:TextView, catlocation: TextView, catage: TextView,
                       imageloc: ImageView) {
        
    }
}