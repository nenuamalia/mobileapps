package com.example.petsadoption.recycleview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.petsadoption.R
import com.example.petsadoption.model.Cat

class ListCatAdapter (private val listCat: ArrayList<Cat>) : RecyclerView.Adapter<ListCatAdapter.ListViewHolder>() {
    private lateinit var onItemClickCallback: OnItemClickCallback

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvName: TextView = itemView.findViewById(R.id.textViewName)
        var tvRas: TextView = itemView.findViewById(R.id.textViewRas)
        var tvGender: TextView = itemView.findViewById(R.id.textViewGender)
        var tvLokasi: TextView = itemView.findViewById(R.id.textViewLocation)
        var tvAge: TextView = itemView.findViewById(R.id.textViewAge)
        var imgPhoto: ImageView = itemView.findViewById(R.id.imagePhoto)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ListViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_cats, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listCat.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val cat = listCat[position]
        Glide.with(holder.itemView.context)
            .load(cat.photo)
            .into(holder.imgPhoto)
        holder.tvName.text = cat.name
        holder.tvRas.text = cat.ras
        holder.tvGender.text = cat.gender
        holder.tvLokasi.text = cat.lokasi
        holder.tvAge.text = cat.age


        holder.itemView.setOnClickListener { onItemClickCallback.onItemClick(listCat[holder.adapterPosition]) }
    }

    interface OnItemClickCallback {
        fun onItemClick(data: Cat)
    }
}