package com.example.petsadoption

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_cat_detail.*

class CatDetail : AppCompatActivity(), View.OnClickListener  {
    companion object{
        const val EXTRA_NAME = "extra_name"
        const val EXTRA_RAS = "extra_ras"
        const val EXTRA_GENDER = "extra_gender"
        const val EXTRA_LOKASI = "extra_lokasi"
        const val EXTRA_PHOTO = "extra_photo"
        const val EXTRA_DETAIL = "extra_detail"
        const val EXTRA_AGE = "extra_age"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cat_detail)

        val btnAdopt: Button = findViewById(R.id.btn_adopt)
        btnAdopt.setOnClickListener(this)

        showCatDetail()
    }

    private fun showCatDetail() {
        textViewName.setText(intent.getStringExtra(EXTRA_NAME))
        textViewRas.setText(intent.getStringExtra(EXTRA_RAS))
        textViewGender.setText(intent.getStringExtra(EXTRA_GENDER))
        textViewLocation.setText(intent.getStringExtra(EXTRA_LOKASI))
        details_desc.setText(intent.getStringExtra(EXTRA_DETAIL))
        textViewAge.setText(intent.getStringExtra(EXTRA_AGE))

        Glide.with(this)
            .load(intent.getIntExtra(EXTRA_PHOTO,0))
            .into(imagePhoto)
    }

    override fun onClick(v: View){
        when(v.id){
            R.id.btn_adopt -> {
                val phoneNumber = "085883618080"
                val dialPhoneIntent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phoneNumber"))
                startActivity(dialPhoneIntent)
            }
        }
    }
}