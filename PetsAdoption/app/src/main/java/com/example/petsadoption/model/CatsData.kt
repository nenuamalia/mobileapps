package com.example.petsadoption.model

import com.example.petsadoption.R
import java.io.Serializable

object CatsData: Serializable{
    private val catNames = arrayOf(
        "Chico", "Lucy", "Kitty", "Pedro", "Shiro",
        "Timmy", "Katie", "Sagwa", "Sigwa", "Agata"
    )

    private val catRas = arrayOf(
        "Persian",
        "Angora",
        "Turkey",
        "Domestic",
        "Persian",
        "Mainecoon",
        "British",
        "Domestic",
        "Angora",
        "Persian"
    )

    private val catGender = arrayOf(
        "Male",
        "Female",
        "Female",
        "Male",
        "Male",
        "Male",
        "Female",
        "Female",
        "Male",
        "Female"
    )

    private val catLokasi = arrayOf(
        "Jakarta",
        "Depok",
        "Bekasi",
        "Tangerang",
        "Bogor",
        "Bandung",
        "Solo",
        "Surabaya",
        "Madiun",
        "Sumedang"
    )

    private val catImages = intArrayOf(
        R.drawable.cat0,
        R.drawable.cat1,
        R.drawable.cat2,
        R.drawable.cat3,
        R.drawable.cat4,
        R.drawable.cat5,
        R.drawable.cat6,
        R.drawable.cat7,
        R.drawable.cat8,
        R.drawable.cat9
    )

    private val catDetail = arrayOf(
        "Berbulu oren, sangat ramah",
        "Berbulu abu, sangat pendiam",
        "Berbulu hitam, sangat lucu",
        "Berbulu oren hitam, sangat aktif",
        "Berbulu oren, sangan lucu",
        "Berbulu belang, sangat aktif",
        "Berbulu abu tua, sangat lincah",
        "Berbulu hitam, sangat pendiam",
        "Berbulu belang, sangat patuh",
        "Berbulu putih abu, sangat gembul"
    )

    private val catAge = arrayOf(
        "1.5 years",
        "1 years",
        "1.2 years",
        "1.4 years",
        "2 years",
        "1 years",
        "9 months",
        "7 months",
        "1 years",
        "2 years"
    )

    val listData: ArrayList<Cat>
        get() {
            val list = arrayListOf<Cat>()
            for(position in catNames.indices){
                val cat = Cat()
                cat.name = catNames[position]
                cat.ras = catRas[position]
                cat.gender = catGender[position]
                cat.lokasi = catLokasi[position]
                cat.photo = catImages[position]
                cat.detail = catDetail[position]
                cat.age = catAge[position]
                list.add(cat)
            }
            return list
        }
}