package com.example.petsadoption

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView

class MainActivity : AppCompatActivity() {
    private lateinit var iv: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        iv = findViewById(R.id.iv)
        val myanim = AnimationUtils.loadAnimation(this,R.anim.mytransition)
        iv.startAnimation(myanim)
        val Dashboard = Intent(this@MainActivity, Dashboard::class.java)

        val timer = Thread {
            try {
                Thread.sleep(5000)
            }
            catch (e: InterruptedException) {
               e.printStackTrace()
            }
            finally {
                startActivity(Dashboard)
                finish()
            }
        }

        timer.start()

    }
}