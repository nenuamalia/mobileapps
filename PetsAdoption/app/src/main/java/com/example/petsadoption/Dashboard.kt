package com.example.petsadoption

import android.app.ActivityOptions
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.util.Pair
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.petsadoption.model.Cat
import com.example.petsadoption.model.CatsData
import com.example.petsadoption.recycleview.CatCallback
import com.example.petsadoption.recycleview.ListCatAdapter

class Dashboard : AppCompatActivity(), CatCallback {
    private lateinit var rvCats: RecyclerView
    private var list: ArrayList<Cat> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        rvCats = findViewById(R.id.rv_cats)
        rvCats.setHasFixedSize(true)

        list.addAll(CatsData.listData)
        showRecyclerList()

        val aboutMe = findViewById<ImageView>(R.id.mypoto)
        aboutMe.setOnClickListener {
            startActivity(Intent(this@Dashboard, AboutMe::class.java))
        }

    }

    private fun showRecyclerList(){
        rvCats.layoutManager = LinearLayoutManager(this)
        val listCatAdapter = ListCatAdapter(list)
        rvCats.adapter = listCatAdapter

        listCatAdapter.setOnItemClickCallback(object : ListCatAdapter.OnItemClickCallback {
            override fun onItemClick(data: Cat) {
                val manageDetailIntent = Intent(this@Dashboard, CatDetail::class.java).apply {
                    putExtra(CatDetail.EXTRA_NAME, data.name)
                    putExtra(CatDetail.EXTRA_RAS, data.ras)
                    putExtra(CatDetail.EXTRA_GENDER, data.gender)
                    putExtra(CatDetail.EXTRA_LOKASI, data.lokasi)
                    putExtra(CatDetail.EXTRA_PHOTO, data.photo)
                    putExtra(CatDetail.EXTRA_DETAIL, data.detail)
                    putExtra(CatDetail.EXTRA_AGE, data.age)
                }
                startActivity(manageDetailIntent)
            }
        })
    }

}